# Animal Mobile App - Ionic React Version

The goal of is to learn more about the following:
- Testing (manual and automated)
- Styling & Design
- API integration
- Reuse of components (Storybook)
- Typing (Typescript/Dart)
- Library Integration
- Dependancy & Updating
- Data Storage

## Setup

``` sh
npm install -g @ionic/cli native-run cordova-res

npm install 
```

## Start

``` sh
ionic serve
```

Runs app in default browser - but Chrome(Blink) or Saffari(Webkit) or Gnome Epiphany(Webkit) should probably be used if that isn't your default, as that is closer to the webviews ionic would use on Android or iOS. 
A new tab should automatically open and listen for updates - same as most webpack projects these days basically 

Use responsive mode in dev tools to get a feel for layout. 

To deploy to a device - stop the `ionic serve` process.
```
ionic build
ionic cap run android 
// or
ionic cap run ios
```

`cap` stands for [Capacitor](https://capacitorjs.com/docs) - a "spiritual successor to Apache Cordova and Adobe PhoneGap, with inspiration from other popular cross-platform tools like React Native and Turbolinks, but focused entirely on enabling modern web apps to run on all major platforms with ease."

to run with hot reloading on device or emulator use
```
ionic cap run android -l --external
```