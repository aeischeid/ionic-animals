import React, {useState} from 'react';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonList, IonItem, IonLabel, IonBadge, IonNote, IonButton, IonIcon, IonModal } from '@ionic/react';
import { useAnimalStore, Animal } from '../hooks/useAnimalStore';
import './Records.css';
import { refreshOutline } from 'ionicons/icons';
import AnimalDetail from '../components/AnimalDetail';
import { useHistory } from "react-router-dom";

const emptyAnimal:Animal = {name:'', species:'', gender:''}

const Records: React.FC = () => {
  const [animal, setAnimal] = useState<Animal>(emptyAnimal);
  const { fetchAnimals, animals } = useAnimalStore();
  let history = useHistory();

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Records</IonTitle>
          <IonButton slot="end" onClick={fetchAnimals}>
            <IonIcon icon={refreshOutline} />
          </IonButton>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonList>
          {animals.map((animal, index) => (
            <IonItem key={index} onClick={()=>setAnimal(animal)}>
              <IonLabel>
                <h1>{animal.name}</h1>
                <IonNote>{animal.species}</IonNote>
              </IonLabel>
              <IonBadge color="success" slot="end">
                {animal.gender}
              </IonBadge>
            </IonItem>
          ))}
        </IonList>
        <IonModal isOpen={!!animal.id} cssClass='my-custom-class'>
          <AnimalDetail animal={animal} />
          <IonButton color="secondary" onClick={()=> history.push('/edit')}>Edit</IonButton>
          <IonButton onClick={() => setAnimal(emptyAnimal)}>Close</IonButton>
        </IonModal>
      </IonContent>
    </IonPage>
  );
};

export default Records;
