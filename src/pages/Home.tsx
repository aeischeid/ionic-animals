import React, {useState} from 'react';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonSearchbar, IonGrid, IonRow, IonCol, IonButton, IonIcon, IonListHeader, IonList, IonItem, IonLabel, IonBadge, IonNote } from '@ionic/react';
import './Home.css';
import { bugOutline, personAdd } from 'ionicons/icons';

const Home: React.FC = () => {
  const [searchText, setSearchText] = useState('');

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Home</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Wecome!</IonTitle>
          </IonToolbar>
          <IonToolbar>
            <IonSearchbar value={searchText} onIonChange={e => setSearchText(e.detail.value!)}></IonSearchbar>
          </IonToolbar> 
        </IonHeader>
        <IonGrid>
          <IonRow>
            <IonCol size="6">
              <IonButton expand="block">
                <IonIcon slot="start" icon={bugOutline} />
                Create an animal
              </IonButton>
            </IonCol>
            <IonCol size="6">
              <IonButton expand="block">
                <IonIcon slot="start" icon={personAdd} />
                Create a contact
              </IonButton>
            </IonCol>
          </IonRow>
        </IonGrid>
        <IonList inset={true} lines="inset">
          <IonListHeader>Recently updated animal records</IonListHeader>
          <IonItem>
            <IonLabel>
              <h1>Friendly One</h1>
              <IonNote>Equine</IonNote>
            </IonLabel>
            <IonBadge color="success" slot="end">
              Gelding
            </IonBadge>
          </IonItem>
          <IonItem>
            <IonLabel>
              <h1>Happy Too</h1>
              <IonNote>Canine</IonNote>
            </IonLabel>
            <IonBadge color="success" slot="end">
              Female
            </IonBadge>
          </IonItem>
        </IonList>
      </IonContent>
    </IonPage>
  );
};

export default Home;
