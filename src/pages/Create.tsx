import React, {useState} from 'react';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonItem, IonInput, IonLabel, IonButton, IonSelect, IonSelectOption, IonGrid, IonRow, IonIcon, IonCol, IonImg, IonActionSheet, IonCheckbox } from '@ionic/react';
import { camera, close, trash } from 'ionicons/icons';
import { usePhotoGallery, Photo } from '../hooks/usePhotoGallery';
import { Animal } from '../hooks/useAnimalStore';
import './Create.css';

interface ContainerProps {
	animal?: Animal;
}

const Create: React.FC<ContainerProps> = ({animal}) => {
  const [species, setSpecies] = useState<string>();
  const [name, setName] = useState<string>();
  const [gender, setGender] = useState<string>();
  const [customAge, setCustomAge] = useState<boolean>();
  const [photoToDelete, setPhotoToDelete] = useState<Photo>();
  const { deletePhoto, photos, takePhoto } = usePhotoGallery();

  if (animal) {console.log(animal)}

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Create</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Create a new</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonItem>
          <IonLabel position="stacked">Species</IonLabel>
          <IonInput value={species} onIonChange={e => setSpecies(e.detail.value!)}></IonInput>
        </IonItem>
        <IonItem>
          <IonLabel position="stacked">Name</IonLabel>
          <IonInput value={name} onIonChange={e => setName(e.detail.value!)}></IonInput>
        </IonItem>

        <IonItem>
          <IonLabel position="stacked">Gender</IonLabel>
          <IonSelect value={gender} placeholder="Select One" onIonChange={e => setGender(e.detail.value)}>
            <IonSelectOption value="female">Female</IonSelectOption>
            <IonSelectOption value="male">Male</IonSelectOption>
          </IonSelect>
        </IonItem>
        <IonItem>
            <IonCheckbox checked={customAge} onIonChange={e => setCustomAge(e.detail.checked)} mode="md" color="primary" slot="start"/>
            <IonLabel>Enter a text description for age</IonLabel>
          </IonItem>
        <IonGrid>
          <IonRow>
            {photos.map((photo, index) => (
              <IonCol size="6" key={index}>
                <IonImg
                  onClick={() => setPhotoToDelete(photo)}
                  src={photo.webviewPath}
                />
              </IonCol>
            ))}
            <IonCol>
              <IonButton onClick={() => takePhoto()}>
                <IonIcon icon={camera}></IonIcon>
              </IonButton>
            </IonCol>
          </IonRow>
        </IonGrid>

        <IonButton expand="block" onClick={() => alert('TODO validation and saving...')}>
          Create animal
        </IonButton>

        <IonActionSheet
          isOpen={!!photoToDelete}
          buttons={[{
            text: 'Delete selected',
            role: 'destructive',
            icon: trash,
            handler: () => {
              if (photoToDelete) {
                deletePhoto(photoToDelete);
                setPhotoToDelete(undefined);
              }
            }
          }, {
            text: 'Cancel',
            icon: close,
            role: 'cancel'
          }]}
          onDidDismiss={() => setPhotoToDelete(undefined)}
        />

      </IonContent>
    </IonPage>
  );
};

export default Create;
