import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import {
  IonApp,
  IonIcon,
  IonLabel,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonTabs
} from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import { addOutline, folderOutline, notificationsOutline, person, reorderThreeOutline } from 'ionicons/icons';
import Home from './pages/Home';
import Records from './pages/Records';
import Create from './pages/Create';
import Alerts from './pages/Alerts';
import Profile from './pages/Profile';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';

const App: React.FC = () => (
  <IonApp>
    <IonReactRouter>
      <IonTabs>
        <IonRouterOutlet>
          <Route path="/home" component={Home} exact={true} />
          <Route path="/records" component={Records} exact={true} />
          <Route path="/create" component={Create} />
          <Route path="/edit" component={Create} />
          <Route path="/alerts" component={Alerts} exact={true} />
          <Route path="/profile" component={Profile} exact={true} />
          <Route path="/" render={() => <Redirect to="/home" />} exact={true} />
        </IonRouterOutlet>
        <IonTabBar slot="bottom">
          <IonTabButton tab="home" href="/home">
            <IonIcon icon={reorderThreeOutline} />
            <IonLabel>Menu</IonLabel>
          </IonTabButton>
          <IonTabButton tab="records" href="/records">
            <IonIcon icon={folderOutline} />
            <IonLabel>Records</IonLabel>
          </IonTabButton>
          <IonTabButton tab="tab3" href="/create">
            <IonIcon icon={addOutline} />
            <IonLabel>Create</IonLabel>
          </IonTabButton>
          <IonTabButton tab="alerts" href="/alerts">
            <IonIcon icon={notificationsOutline} />
            <IonLabel>Alerts</IonLabel>
          </IonTabButton>
          <IonTabButton tab="profile" href="/profile">
            <IonIcon icon={person} />
            <IonLabel>Profile</IonLabel>
          </IonTabButton>
        </IonTabBar>
      </IonTabs>
    </IonReactRouter>
  </IonApp>
);

export default App;
