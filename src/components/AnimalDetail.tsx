import React from 'react';
import './AnimalDetail.css';
import { Animal } from '../hooks/useAnimalStore';

interface ContainerProps {
	animal: Animal;
}

// a demonstration of using plain ol html - no Ionic elements
const AnimalDetail: React.FC<ContainerProps> = ({ animal }) => {
	return (
		<div className="animal-detail">
			<h2>Animal Detail</h2>
			<h3>Name: {animal.name}</h3>
			<dl>
				<dd>Species</dd>
				<dt>{animal.species}</dt>
				<dd>Gender</dd>
				<dt>{animal.gender}</dt>
			</dl>
		</div>
	);
};

export default AnimalDetail;
