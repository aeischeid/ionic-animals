import { useState, useEffect } from "react";
import { useStorage } from '@ionic/react-hooks/storage';

// This acts sort of like a model / domain
export interface Animal {
	id?: number;
	name: string;
	species: string;
	gender: string;
	age?: string;
	dob?: string; // could be date?
}

const ANIMAL_STORAGE = "animals";

// userToken should be a global set by login or gotten from app storage.
const userToken = 'someFakeToken'
// const userToken = 'vet1Token'
// baseUrl would ideally be an env or config var.
const baseUrl = 'https://staging.globalvetlink.com/gvl2/'
// const baseUrl = 'https://testing.globalvetlink.com/gvl2/'


export function useAnimalStore() {
	const [animals, setAnimals] = useState<Animal[]>([]);
	
	// const { deleteFile, getUri, readFile, writeFile } = useFilesystem();
	const { get, set } = useStorage();

	useEffect(() => {
		const loadAnimals = async () => {
			const animalsString = await get('animals');
			const animalsInStorage = (animalsString ? JSON.parse(animalsString) : []) as Animal[];
			setAnimals(animalsInStorage);
		};
		loadAnimals();
		// If have a internet connection...
		// TODO:
		// pushUpdates();
		// getUpdates();
	}, [get]);

	const syncAnimal = async (animal: Animal, action: string): Promise<Animal> => {
		let url: string = `${baseUrl}api/animal`
		if (action !== 'POST') {
			url += `/${animal.id}`
		}
		const response = await fetch(
			url,
			{
				method: action,
				headers: {
					'x-auth-token': userToken,
				},
			}
		);
		if (!response.ok) {
			return Promise.resolve(animal)
		} else {
			return Promise.resolve(animal)
		} 
	}

	const saveAnimal = async (animal: Animal): Promise<Animal> => {
		let syncedAnimal = await syncAnimal(animal, 'POST')
		const newAnimals = [syncedAnimal, ...animals];
		setAnimals(newAnimals);
		set(ANIMAL_STORAGE, JSON.stringify(animals));
		
		return syncedAnimal
	};

	const updateAnimal = async (animal: Animal): Promise<Animal> => {
		// find this animals and temporarily remove it from the reference data array
		const previousVersionAnimal = animals.find(p => p.id === animal.id);
		const otherAnimals = animals.filter(p => p.id !== animal.id);

		// TODO: validation and save animal?
		let syncedAnimal = await syncAnimal(animal, 'PUT')
		const newAnimals = [syncedAnimal, ...otherAnimals];
	
		// Update animals array cache by overwriting the existing animal array
		set(ANIMAL_STORAGE, JSON.stringify(newAnimals));
		setAnimals(newAnimals);
		return syncedAnimal
	};

	const deleteAnimal = async (animal: Animal) => {
		// Remove this animal from the Animals reference data array
		const newAnimals = animals.filter(p => p.id !== animal.id);

		await syncAnimal(animal, 'DELETE')
	
		// Update animals array cache by overwriting the existing animal array
		set(ANIMAL_STORAGE, JSON.stringify(newAnimals));
		setAnimals(newAnimals);
	};

	const fetchAnimals = async (): Promise<Animal[]> => {

		let animalListEndPoint = `localhost:3000/assets/json/animals.json`;
		/* let animalListEndPoint = `${baseUrl}api/animal`
		const response = await fetch(
			animalListEndPoint,
			{
				method: 'GET',
				headers: {
					'auth-token': userToken,
				},
			}
		);

		if (!response.ok) {
			throw new Error(response.statusText);
		}
		console.log(response);
		let fetchedAnimals: Animal[] = await response.json() as Animal[];
		// let tempAnimal: Animal = {name:'fake', species:'fake', gender:'fake'};
		// for (let animal of response.json()) {
		// 	tempAnimal = animal as Animal;
		// 	fetchedAnimals.push(tempAnimal);
		// }
		*/

		let fetchedAnimals = require('../json/animals.json') as Animal[]
	
		// Update animals array cache by overwriting the existing animal array
		set(ANIMAL_STORAGE, JSON.stringify(fetchedAnimals));
		setAnimals(fetchedAnimals);
		return fetchedAnimals;
	};

	return {
		deleteAnimal,
		saveAnimal,
		updateAnimal,
		fetchAnimals,
		animals,
	};
}